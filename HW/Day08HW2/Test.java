import java.util.InputMismatchException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.io.File;
import java.io.PrintWriter;

public class Test {
	static ArrayList<Airport> airportList = new ArrayList<>();
	static Scanner input = new Scanner(System.in);
	static final String TEXT_FILE = "airports.txt";

	public static void main(String[] args){
		readDataFromFile();

		while(true){
			int choice = getMenuChoice();
			System.out.println();

			switch(choice){
				case 1:
					showAllAirport();
					break;
				case 2:
					findDistance();
					break;
				case 3:
					findNearest();
					break;
				case 4:
					addNewAirport();
					break;
				case 0:
					saveDataToFile();
					System.out.println("ByeBye~");
					return;
				default :
					System.out.println("Choose one of menu above");
			}
			System.out.println();
		}
	}
	static int getMenuChoice(){
		System.out.print(
				"1. Show all airports (codes and city names)\n" +
				"2. Find distance between two airports by codes.\n" +
				"3. Find the 1 airport nearest to an airport given and display the distance.\n" +
				"4. Add a new airport to the list.\n" +
				"0. Exit.\n" +
			    "Enter number: ");

		int choice = inputInt();
		return choice;
	}

	static void addNewAirport(){
		try{
			System.out.println("Add new airport");

			System.out.print("Enter Code: ");
			String code = input.nextLine();

			System.out.print("Enter City: ");
			String city= input.nextLine();

			System.out.print("Enter Latitude: ");
			double latitude = inputDouble();
			System.out.print("Enter Longitude: ");
			double longitude = inputDouble();

			airportList.add(new Airport(code,city,latitude,longitude));
		} catch (ParameterInvalidException ex){
			System.out.println();
			System.out.println(ex.getMessage());
		}
	}

	static void findNearest(){
		if(airportList.size() <2){
			System.out.println("Not enough airport in the list");
			return;
		}

		System.out.println("Find the nearest");
		System.out.print("Enter number of airport: ");
		int airportNum= inputInt();

		if( airportNum > airportList.size() || airportNum < 0){
			System.out.println("Invalid number: "+airportNum);
			return;
		}

		Airport airport = airportList.get(airportNum-1);
		double nearest = Double.MAX_VALUE;
		Airport nearestAir = airport;
		for(int i=0; i < airportList.size(); i++){
			if(i == airportNum-1){
				continue;
			}
			double dist = distance(airport.getLatitude()
					,airportList.get(i).getLatitude()
					,airport.getLongitude()
					,airportList.get(i).getLongitude()
					,0,0);
			if(dist < nearest ){
				nearestAir = airportList.get(i);
				nearest = dist;
			}
		}

		System.out.printf("The nearest airport from %s[%s] is %s[%s], distance: %,.2fkm\n"
							,airport.getCode(),airport.getCity(),nearestAir.getCode(),nearestAir.getCity(),nearest/1000);
	}

	static void findDistance(){
		if(airportList.size() <2){
			System.out.println("Not enough airport in the list");
			return;
		}

		System.out.println("Find the distance");
		System.out.print("Enter number of airport1: ");
		int ap1 = inputInt();
		System.out.print("Enter number of airport2: ");
		int ap2 = inputInt();

		if(ap1 == ap2){
			System.out.println("You choose same airport");
			return;
		}
		if(ap1 > airportList.size() || ap2 > airportList.size()
			|| ap1 <= 0 || ap2 <= 0 ) {
			System.out.println("Invalid list number");
			return;
		}

		Airport air1 = airportList.get(ap1-1);
		Airport air2 = airportList.get(ap2-1);

		double disAirports = distance(air1.getLatitude(), air2.getLatitude(),
						air1.getLongitude(), air2.getLongitude(),0,0);

		System.out.printf("The distance between %s Airport[%s] and %s Airport[%s] is %,.2fkm\n",air1.getCode(),air1.getCity(),air2.getCode(),air2.getCity(),disAirports/1000);

	}

	static void showAllAirport(){
		for(int i=0; i < airportList.size(); i++){
			System.out.printf("#%d: %s\n",i+1,airportList.get(i));
		}
	}

	static int inputInt(){
		while(true){
			try{
				int choice = input.nextInt();
				input.nextLine();
				return choice;
			} catch(InputMismatchException ex) {
				System.out.print("Invalid menu. Try again: ");
				input.nextLine();
			}
		}
	}

	static double inputDouble(){
		while(true){
			try{
				double number = input.nextDouble();
				input.nextLine();
				return number;
			} catch(InputMismatchException ex) {
				System.out.print("Invalid Number. Try again: ");
				input.nextLine();
			}
		}
	}

	 static double distance(double lat1, double lat2, double lon1,
		        double lon2, double el1, double el2) {

	    final int R = 6371; // Radius of the earth

		double latDistance = Math.toRadians(lat2 - lat1);
		double lonDistance = Math.toRadians(lon2 - lon1);
		double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2)
					+ Math.cos(Math.toRadians(lat1)) 
					* Math.cos(Math.toRadians(lat2))
					* Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		double distance = R * c * 1000; // convert to meters

		double height = el1 - el2;

		distance = Math.pow(distance, 2) + Math.pow(height, 2);

		return Math.sqrt(distance);
	}

	static void readDataFromFile(){
		try(Scanner fileInput = new Scanner(new File(TEXT_FILE))){
			while(fileInput.hasNextLine()){
				try{
					String dataLine = fileInput.nextLine();
					airportList.add(new Airport(dataLine));

				} catch (ParameterInvalidException ex){
					System.out.println(ex.getMessage());
				}
			}
		} catch (IOException ex){
			System.out.println("Cannot read the file");
		}
	}
	static void saveDataToFile(){
		try(PrintWriter fileOutput = new PrintWriter(new File("_" +TEXT_FILE))){
			for(Airport air : airportList){
				fileOutput.println(air.toFileString());
			}
		}catch(IOException ex){
			System.out.println("Cannot save your Data");
		}
	}

}

class ParameterInvalidException extends RuntimeException {
	public ParameterInvalidException(String message){
		super(message);
	}
}
