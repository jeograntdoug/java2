/*
Day04SimpleCalc (BONUS)
-----------------------

Ask user to choose an option from menu:
1. Add
2. Subtract
3. Multiply
4. Divide

If user enters an invalid choice, other than 1-4, display error message and exit.

Otherwise ask user for 2 floating point values,
perform the requested computation and display the result.

BONUS:

Add one more option to the menu.
0. Exit

And add a loop around the entire program so that
the menu is displayed repeatedly until user asks to exit.

Suggestion: instead of do-while loop use
an infinite loop and a break/return.
 */
package hwday04simplecalc;

import java.util.Scanner;

public class HWDay04SimpleCalc {

    public static void main(String[] args) {
       final int EXIT = 0;
       final int ADD = 1;
       final int SUBSTRACT = 2;
       final int MULTIPLY = 3;
       final int DIVIDE = 4;
       
       String str;
       String operator = " ";
       int menu;
       float[] point = new float[2];
       

       Scanner input = new Scanner(System.in);
       
        System.out.printf("Choose an option from menu : \n0. Exit\n1. Add\n2. Subtract\n3. Multiply\n4. Divide\n");
        
        str = input.nextLine();
        menu = Integer.valueOf(str);
        
        if( menu == EXIT) {
            System.out.println("ByeBye~");
            return;
        }
        
        // Invalid Menu
        if( !(menu ==ADD
                || menu == SUBSTRACT
                || menu == MULTIPLY
                || menu == DIVIDE) ){
            System.out.println("ERROR : Invalid Menu!");
            return;
        }
        
        for (int i = 0; i < point.length; i++) {
            System.out.print("Enter Float number: ");
            point[i] = input.nextFloat();
            input.nextLine();
        }
        
        float result = 0; 
        switch(menu){
            case ADD:
                result = point[0] + point[1];
                operator = "+";
                break;
            case SUBSTRACT :
                result = point[0] - point[1];
                operator = "-";
                break;
            case MULTIPLY :
                result = point[0] * point[1];
                operator = "*";
                break;
            case DIVIDE :
                result = point[0] / point[1];
                operator = "/";
                break;
        }
        
        System.out.printf("%4.2f %s %4.2f = %4.2f",point[0],operator,point[1],result);
        
        
    }
    
}
