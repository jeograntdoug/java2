public class Point extends GeoObj {
	public Point (String color) throws DataInvalidException {
		super(color);
	}	

	@Override
	public double getSurface(){
		return 0;
	}

	@Override
	public double getCircumPerium(){
		return 0;
	}

	@Override
	public int getVericesCount(){
		return 0;
	}

	@Override
	public int getEdgesCount(){
		return 1;
	}

	@Override
	public String toString(){
		return String.format("Point : %s color, Surface: %.2f Perimater : %.2f Verices: %d Edges: %d",getColor(),getSurface(),getCircumPerium(),getVericesCount(),getEdgesCount());
	}
}
