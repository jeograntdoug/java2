public class Square extends Rectangle{

	Square (String color,double length) throws DataInvalidException{
		super(color,length,length);
	}	

	public void setLength(double length){
		setWidth(length);
		setHeight(length);
	}
	public double getLength(){
		return getHeight();
	}

	@Override
	public String toString(){
		return String.format("Square: %s color, Length: %.2f, Surface: %.2f Perimater : %.2f Verices: %d Edges: %d",getColor(),getWidth(),getSurface(),getCircumPerium(),getVericesCount(),getEdgesCount());
	}
}

