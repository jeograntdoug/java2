import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import java.util.ArrayList;

public class Test {

	static final String TEXT_FILE = "objects.txt";
	static ArrayList<GeoObj> geoList = new ArrayList<>();

	public static void main(String[] args){
		try(Scanner fileInput =  new Scanner(new File(TEXT_FILE))) {
			while(fileInput.hasNextLine()){
				try{
					GeoObj geoObj = GeoObj.createFromLine(fileInput.nextLine());
					geoList.add(geoObj);
				} catch ( DataInvalidException ex) {
					System.out.println(ex.getMessage());
				}
			}

			for(GeoObj geo : geoList){
				System.out.println(geo);
			}

		} catch (IOException ex) {
			System.out.println("Cannot open the file! Good bye");
			return;
		} 
	}
}
