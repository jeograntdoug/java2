public class Rectangle extends GeoObj {
	private double width;
	private double height;

	public Rectangle (String color,double width, double height) throws DataInvalidException {
		super(color);
		setWidth(width);
		setHeight(height);
	}	

	public double getWidth(){
		return width;
	}
	public double getHeight(){
		return height;
	}
	public void setWidth(double width){
		this.width = width;
	}
	public void setHeight(double height){
		this.height = height;
	}

	@Override
	public double getSurface(){
		return width * height;
	}

	@Override
	public double getCircumPerium(){
		return 2*(width+height);
	}

	@Override
	public int getVericesCount(){
		return 4;
	}

	@Override
	public int getEdgesCount(){
		return 4;
	}

	@Override
	public String toString(){
		return String.format("Rectangle: %s color, Width: %.2f, Height: %.2f, Surface: %.2f Perimater : %.2f Verices: %d Edges: %d",getColor(),width,height,getSurface(),getCircumPerium(),getVericesCount(),getEdgesCount());
	}
}
