/*
 * 
 */
import java.util.ArrayList;

public class Day06RandNums {
	public static void main(String[] args){
		RandomStore rs = new RandomStore();
		int v1 = rs.nextInt(1, 10);
		int v2 = rs.nextInt(-100, -10);
		int v3 = rs.nextInt(-20,21);
		System.out.printf("Values %d, %d, %d\n", v1, v2, v3);
		rs.printHistory();
	}
}

class RandomStore {
	ArrayList<Integer> intHistory;

	public RandomStore(){
		intHistory = new ArrayList<>();
	}

	int nextInt(int minIncl,int maxExcl) {

		int random = (int)(Math.random()*(maxExcl-minIncl) +minIncl);
		intHistory.add(random);
		return random;
	}
	void printHistory() {

		int size = intHistory.size();
		for(int i=0; i < size; i++){
			System.out.printf("%s%d",i==0?"":", ",intHistory.get(i));
		}
		System.out.println();
	}
}
