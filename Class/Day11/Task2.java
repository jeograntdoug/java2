import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Task2 {

    static HashMap<String, ArrayList<String>> map = new HashMap<>();

    public static void main(String[] args){
        try(Scanner fileInput = new Scanner(new File("teams.txt"));) {

            while(fileInput.hasNextLine()){

                String dataLine = fileInput.nextLine();
                String[] teamAndMember= dataLine.split(":");

                if(teamAndMember.length !=2){
                    System.out.println("Invalid data line: "+dataLine);
                    continue;
                }

                String teamName = teamAndMember[0];
                String teamMembers = teamAndMember[1];

                String[] members = teamMembers.split(",");

                if(members == null){
                    System.out.println("There is no member: "+dataLine);
                    continue;
                }


                for(String member : members){
                    if(!map.containsKey(member)){
                        map.put(member,new ArrayList<String>());
                    }

                    ArrayList<String> teamList = map.get(member);
                    teamList.add(teamName);
                }
            }

            print(map);

        } catch (IOException ex){
            System.out.println("Cannot open the file");
            return;
        }
    }

    static void print(HashMap<String, ArrayList<String>> map){
        if(map.isEmpty()){
            System.out.println("map is empty");
            return;
        }
        String[] members = map.keySet().toArray(new String[0]);
        for(String member : members){
            System.out.printf("%s plays in: ",member);

            ArrayList<String> teamList = map.get(member);
            int count = 0;
            for(String team : teamList){
                System.out.printf("%s%s",count==0?"":", ",team);
                count++;
            }
            System.out.println();
        }
    }
}
