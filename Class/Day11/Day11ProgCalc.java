import java.util.ArrayList;
import java.util.Scanner;
import java.io.File;
import java.io.IOException;

public class Day11ProgCalc {

    static LIFO<Double> stack = new LIFO<>();
    static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        try(Scanner fileInput = new Scanner(new File("program.txt"))){
            while(fileInput.hasNextLine()){
                String dataLine = fileInput.nextLine();

                try {
                    double number = Double.parseDouble(dataLine);
                    stack.push(number);
                    continue;
                } catch (NumberFormatException ex){
                    //Do noting
                }

                if(dataLine.charAt(0) == '?') {
                    String[] data = dataLine.split(":");
                    if(data.length != 2) {
                        throw new IllegalArgumentException(dataLine);
                    }
                    System.out.printf("%s: ",data[1]);
                    double num = inputDouble();
                    stack.push(num);
                    continue;
                }

                double num1, num2;

                switch (dataLine) {
                    case "+":
                         num1 = stack.pop();
                         num2 = stack.pop();
                        stack.push(num1 + num2);
                        continue;
                    case "-":
                         num1 = stack.pop();
                         num2 = stack.pop();
                        stack.push(num1 - num2);
                        continue;
                    case "*":
                        num1 = stack.pop();
                        num2 = stack.pop();
                        stack.push(num1 * num2);
                        continue;
                    case "/":
                        num1 = stack.pop();
                        num2 = stack.pop();
                        if (num2 == 0) {
                            throw new IllegalArgumentException(dataLine);
                        }
                        stack.push(num2 / num1);
                        continue;
                    case "=":
                        System.out.println(stack.peek());
                        continue;
                    case "pop":
                        stack.pop();
                        continue;
                    default :
                        System.out.println("warning: invalid line, skipping");
                }
            }
        } catch(IOException ex){
            System.out.println("Cannot open the file");
        } catch(IllegalArgumentException ex) {
            System.out.println("Invalid line in the file: "+ex.getMessage());
        }

    }

    static double inputDouble(){
        while(true) {
            try {
                double number = input.nextDouble();
                input.nextLine();

                return number;
            } catch (NumberFormatException ex) {
                input.nextLine();
                System.out.print("Invalid number, try again: ");
            }
        }
    }

}
