package day05treeloops;

import java.util.Scanner;

public class Day05TreeLoops {

    static Scanner input = new Scanner(System.in);
    
    public static void main(String[] args) {
        int treeSize = 0;
        System.out.printf("How big does your tree needs to be? ");
        treeSize = input.nextInt();
        
        System.out.println("Number 1 :");
        int maxDot = 2*treeSize-1;
        
        for (int i = 1; i < treeSize+1; i++) {
            int numOfDot = 2*i-1;
            int blank = (maxDot-numOfDot)/2;
            
            for(int j=0; j < blank; j++ )
                System.out.print(" ");
            
            for(int j=0; j < numOfDot; j++)
                System.out.print("*");
            /* Not nessecery
            for(int j=0; j < blank; j++ )
                System.out.print(" ");
            */
            System.out.println("");
            
        }
        System.out.println("============");
        System.out.println("Number 2 :");

        for(int i=1; i <= treeSize ; i++){
            int numOfDot = 2*i - 1;
            int blank = (maxDot-numOfDot)/2;

            int dotStartIndex = blank;
            int dotEndIndex = blank+ numOfDot-1;

            for(int column=0; column < maxDot; column++){
                System.out.print(
                    (column >= dotStartIndex 
                        && column <= dotEndIndex)? "*":" "
                );
            }
            System.out.println();
        }


        System.out.println("============");
        System.out.println("Number 3 :");
		for(int line=1; line <= treeSize; line++){
			for(int col=0; col < treeSize-line; col++){
				System.out.print(" ");
			}
			for(int col=0; col < line; col++){
				System.out.print("*");
			}
			for(int col=0; col < line-1; col++){
				System.out.print("*");
			}
			System.out.println();
		}
    }
    
}
