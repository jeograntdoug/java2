/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day04randoms;

/**
 *
 * @author 1898918
 */

import java.util.Scanner;
        
public class Day04Randoms {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int max;    // Maximun number
        int min;    // Minimun number
        double random;  // random number from Math.random
        
        Scanner input = new Scanner(System.in);
        
        //ask input
        System.out.print("Enter Max Integer : ");
        max = input.nextInt();
        System.out.print("Enter Min Integer : ");
        min = input.nextInt();
        
        // Compare minimum value to maximum value
        // If minimum value is greater than maximum value print Error then program exit
        if(max < 0 || min < 0 || min > max) {
            System.out.println("Error: Max is smaller than Min");
            return; // System.exit(1);
        }
        // Print maximum value and minimum value
        System.out.println("10 numbers between max:"+ max + " min:"+min);
        
        
        for(int i=0; i<10 ; i++){
            random =  Math.random();    // Generate random number every time
            /* By multiple random number( 0<= random <1) with max-min+1*/
            System.out.println((int)(random * (max-min + 1))+min);
            
        }
    } 
}
