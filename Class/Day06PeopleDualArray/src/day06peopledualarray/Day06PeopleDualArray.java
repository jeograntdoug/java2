/*
Declare two arrays:

String[] namesArray = new String[4];
int[] agesArray = new int[4];

Ask user for names and ages of four people and save the information in the arrays.

Find the youngest person and display their name and age.
 */
package day06peopledualarray;

import java.util.Scanner;

public class Day06PeopleDualArray {

    static Scanner input = new Scanner(System.in);
    
    public static void main(String[] args) {
        String[] namesArray = new String[4];
        int[] agesArray = new int[4];
        
        for (int i = 0; i < namesArray.length; i++) {
            System.out.printf("Enter name of person %d#: ",i+1);
            namesArray[i] = input.nextLine();
            System.out.printf("Enter age  of person %d#: ",i+1);
            agesArray[i] = input.nextInt();
            input.nextLine();
        }
        System.out.println("");
        
        // Find minimux age index
        int minAgeIndex=0;
        int minAge = agesArray[0];
        for (int i = 1; i < namesArray.length; i++) {
            if(agesArray[i] < minAge){
                minAge = agesArray[i];
                minAgeIndex = i;
            }
        }
        System.out.printf("Youngest person is %d and their name is %s\n",agesArray[minAgeIndex],namesArray[minAgeIndex]);
        
    }
    
}
