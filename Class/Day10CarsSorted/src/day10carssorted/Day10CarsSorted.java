package day10carssorted;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Day10CarsSorted {

    static ArrayList<Car> parking = new ArrayList<>();

    public static void main(String[] args) {
        parking.add(new Car("Benz", 12.3, 1950));
        parking.add(new Car("Toyota", 11.3, 1950));
        parking.add(new Car("Toyota", 10.3, 2050));
        parking.add(new Car("Cadlac", 15.9, 1950));
        parking.add(new Car("Nissan", 5.3, 2000));
        parking.add(new Car("Benz", 13.0, 1950));
        parking.add(new Car("Canada", 3.0, 1950));

        for (Car car : parking) {
            System.out.println(car);
        }

        Collections.sort(parking);
        System.out.println("Sort by makeModel");
        for (Car car : parking) {
            System.out.println(car);
        }
        System.out.println("Sort by Engine size");
        Collections.sort(parking, Car.CompareByEngine);
        for (Car car : parking) {
            System.out.println(car);
        }

        System.out.println("Sort by Production Year");
        Collections.sort(parking, Car.CompareByYear);
        for (Car car : parking) {
            System.out.println(car);
        }

    }

}

class Car implements Comparable<Car> {

    String makeModel;
    double engineSizeL;
    int prodYear;

    Car(String makeModel, double engineSizeL, int prodYear) {
        this.makeModel = makeModel;
        this.engineSizeL = engineSizeL;
        this.prodYear = prodYear;
    }

    @Override
    public String toString() {
        return String.format("%s: MakeModel: %s, EngineSize: %.2f, Production Year: %d", getClass().getSimpleName(), makeModel, engineSizeL, prodYear);
    }

    @Override
    public int compareTo(Car car) {
        if (this.makeModel.compareTo(car.makeModel) == 0) {
            return CompareByYear.compare(this, car);
        }
        return this.makeModel.compareTo(car.makeModel);
    }

    static Comparator<Car> CompareByYear = new Comparator<Car>() {

        @Override
        public int compare(Car car1, Car car2) {
            int result = car1.prodYear - car2.prodYear;
            if(result != 0) return result;
            return car1.makeModel.compareTo(car2.makeModel);     
            //return car1.compareTo(car2); ==> stack over flow!!!
        }
    };
    
    static Comparator<Car> CompareByEngine = new Comparator<Car>() {
        @Override
        public int compare(Car car1, Car car2) {
            if (car1.engineSizeL > car2.engineSizeL) {
                return 1;
            } else if (car1.engineSizeL < car2.engineSizeL) {
                return -1;
            } else {
                return 0;
            }
        }
    };
}
