package day05arraycontains;

public class Day05ArrayContains {

    public static void main(String[] args) {

        // Part 1 : Concatenate two arrays
        System.out.println("Part1");
        int[] a1 = {2, 7, 8};
        int[] a2 = {-2, 9};

        int[] myArray = concatenate(a1, a2);

        System.out.println("Concatenate Two Array: ");
        System.out.printf("a1: ");
        printArray(a1);
        System.out.printf("a2: ");
        printArray(a2);
        printArray(myArray);
        // End of part 1

        System.out.println();
        System.out.println();

        // Part 2 : Print duplicated elements
        System.out.println("Part2");
        int[] a3 = {1, 3, 7, 8, 2, 7, 9, 11};
        int[] a4 = {3, 8, 7, 5, 13, 5, 12};

        System.out.println("For arrays:");
        printArray(a3);
        printArray(a4);

        // Part 2 Output
        printDups(a3, a4);

        // End of Part 2
        System.out.println();

        // Part 3 : Remove dups from first array
        int[] a5 = {2, 3, 7, 9, 3};
        int[] a6 = {3, 7};

        System.out.print("a5: ");
        printArray(a5);
        System.out.print("a6: ");
        printArray(a6);

        System.out.println("After remove duplicate elements: ");
        printArray(removeDups(a5, a6));
        // End of Part 3

    }

    static int[] removeDups(int[] a1, int[] a2) {
        a1 = removeSelfDups(a1);
        a2 = removeSelfDups(a2);
        int[] dups = getDups(a1, a2);

		int[] tempArray = new int [a1.length];
		int count = 0;
        for (int i = 0; i < a1.length; i++) {

			boolean isDup = false;
            for (int j = 0; j < dups.length; j++) {
                if (a1[i] == dups[j]) {
					isDup = true;
					break;
                }
            }
			if(!isDup){
				tempArray[count] = a1[i];
				count++;
			}
			
        }

        int[] newArray = new int[count];

        for (int i = 0; i < count; i++) {
			newArray[i] = tempArray[i];
        }

        return newArray;

    }

    static void printArray(int[] numArray) {
        boolean isFirst = true;
        for (int num : numArray) {
            System.out.printf("%s%s", isFirst ? "" : ", ", num);
            isFirst = false;
        }
        System.out.println();
    }

    public static int[] concatenate(int[] a1, int[] a2) {
        int size1 = a1.length;
        int size2 = a2.length;

        int[] conArray = new int[size1 + size2];

        for (int i = 0; i < size1; i++) {
            conArray[i] = a1[i];
        }
        for (int i = 0; i < size2; i++) {
            conArray[size1 + i] = a2[i];
        }
        return conArray;
    }

    public static void printDups(int[] a1, int[] a2) {
        int[] dupArray = getDups(a1, a2);
        System.out.println("Output is:");
        printArray(dupArray);
    }

    public static int[] getDups(int[] a1, int[] a2) {
        a1 = removeSelfDups(a1);
        a2 = removeSelfDups(a2);

        int[] tempArray = new int[a2.length];
        int count = 0;
        for (int i = 0; i < a1.length; i++) {
            for (int j = 0; j < a2.length; j++) {
                if (a2[j] == Integer.MAX_VALUE) {
                    continue;
                }

                if (a1[i] == a2[j]) {
                    tempArray[count] = a2[j];
                    count++;
                }
            }
        }

        int[] newArray = new int[count];

        for (int i = 0; i < count; i++) {
            newArray[i] = tempArray[i];
        }
        return newArray;

    }

    public static int[] removeSelfDups(int[] array) {

        int count = 0;
        for (int i = 0; i < array.length; i++) {

            if (array[i] == Integer.MAX_VALUE) {
                continue;
            }

            for (int j = i + 1; j < array.length; j++) {
                if (array[j] == Integer.MAX_VALUE) {
                    continue;
                }

                if (array[i] == array[j]) {
                    array[j] = Integer.MAX_VALUE;
                    count++;
                }
            }
        }

        int[] newArray = new int[array.length - count];
        int index = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == Integer.MAX_VALUE) {
                continue;
            }
            newArray[index] = array[i];
            index++;
        }
        return newArray;
    }
}
