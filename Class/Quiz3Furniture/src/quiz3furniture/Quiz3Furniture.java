package quiz3furniture;

import java.util.ArrayList;
import java.util.Scanner;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;

public class Quiz3Furniture {

    static ArrayList<HomeItem> homeItemsList = new ArrayList<>();

    public static void main(String[] args) {
        //1. Read data from file
        readDataFromFile();
        System.out.println("");

        //2. Display all HomeItems using toString, one per line.
        System.out.println("Display all HomeItems: ");
        for (HomeItem item : homeItemsList) {
            System.out.println(item);
        }
        System.out.println("");
        //3. Display the total number of HomeItems created using HomeItems.getTotalCreated()
        System.out.println("Total number of items: " + HomeItem.getTotalCreated());
        System.out.println("");
        //4. Sort and display all HomeItems according to price, cheapest first.
        Collections.sort(homeItemsList, ComparePrice);
        System.out.println("Display all HomeItems(sort by price: ");
        for (HomeItem item : homeItemsList) {
            System.out.println(item);
        }
        System.out.println("");
        //5. Sort and display all HomeItems according to price, cheapest first, if price is the same then sort items by their desription.
        Collections.sort(homeItemsList, ComparePriceThenDescription);
        System.out.println("Display all HomeItems(sort by price then description: ");
        for (HomeItem item : homeItemsList) {
            System.out.println(item);
        }
        System.out.println("");
        //6. Sort and display only Electronics items ordered by their serial number.
        ArrayList<ElectronicsItem> elecList = new ArrayList<>();
        
        System.out.println("Display all electronics(sort by Serial number): ");
        for (HomeItem item : homeItemsList) {
            if(item.getClass().getSimpleName().equals("ElectronicsItem")){
                elecList.add((ElectronicsItem)item);
            }
        }
        Collections.sort(elecList, CompareElecSerialNumber);
        for (ElectronicsItem item : elecList) {
            System.out.println(item);
        }
        //7. Sort all HomeItems according to price, cheapest first and *append* that list to file "sorted.txt" in the same format it was read. Use toDataString() which should be implemented in 2 child classes of HomeItem.
        try ( FileWriter fileOutput = new FileWriter(new File("sorted.txt"),true)) {
            for (HomeItem hi : homeItemsList) {
                fileOutput.write(hi.toDataString()+"\n");
            }
        } catch (IOException ex) {
            System.out.println("Can't write data in the file sorted.txt. Bye");
            return;
        }
        Collections.sort(homeItemsList, ComparePrice);
    }

    static void readDataFromFile() {
        try ( Scanner fileInput = new Scanner(new File("household.txt"))) {
            while (fileInput.hasNextLine()) {
                try {
                    String dataLine = fileInput.nextLine();
                    homeItemsList.add(HomeItem.createFromDataLine(dataLine));

                } catch (DataInvalidException ex) {
                    System.out.println("Error : " + ex.getMessage());
                }
            }
        } catch (IOException ex) {
            System.out.println("Cannot read household.txt. ByeBye~");
            System.exit(1);
        }
    }
    static Comparator<HomeItem> ComparePrice = new Comparator<>() {
        @Override
        public int compare(HomeItem item1, HomeItem item2) {
            if (item1.getPrice() == item2.getPrice()) {
                return 0;
            } else if (item1.getPrice() > item2.getPrice()) {
                return 1;
            } else {
                return -1;
            }
        }
    };

    static Comparator<HomeItem> ComparePriceThenDescription = new Comparator<>() {
        @Override
        public int compare(HomeItem item1, HomeItem item2) {
            if (item1.getPrice() == item2.getPrice()) {
                return CompareDescription.compare(item1, item2);
            } else if (item1.getPrice() > item2.getPrice()) {
                return 1;
            } else {
                return -1;
            }
        }
    };

    static Comparator<HomeItem> CompareDescription = new Comparator<>() {
        @Override
        public int compare(HomeItem item1, HomeItem item2) {
            return item1.getDescription().compareTo(item2.getDescription());
        }
    };

    static Comparator<ElectronicsItem> CompareElecSerialNumber = new Comparator<>() {
        @Override
        public int compare(ElectronicsItem item1, ElectronicsItem item2) {           
            return item1.getSerialNo() - item2.getSerialNo();
        }
    };

}
