package quiz3furniture;

public class ElectronicsItem extends HomeItem {
    private String model; // 2-50 characters except semicolon ;
    public ElectronicsItem(String price, String description, String model){
        super(price,description);   //DataInvalidException
        setModel(model);    //DataInvalidException
        totalCreated++;
    }

    public String getModel() {
        return model;
    }
    public void setModel(String model) {
        if(model.contains(";") || model.length()<2 || model.length()>50){
            throw new DataInvalidException("Model name is invalid: "+model);
        }
        this.model = model;
    }
        
    @Override
    public String toDataString(){
        return String.join(";","Electronics",String.valueOf(getPrice()),getDescription(),model);
    }
    @Override
    public String toString(){
        return String.format("Electronic model :%s, Serial number: %d, Price: %.2f, Description: %s", model,getSerialNo(),getPrice(),getDescription());
    }

}
