package quiz3furniture;

public class DataInvalidException extends RuntimeException {
    DataInvalidException (String message){
        super(message);
    }
    
}
