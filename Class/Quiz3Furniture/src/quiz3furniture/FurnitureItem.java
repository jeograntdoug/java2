package quiz3furniture;

public class FurnitureItem extends HomeItem {

    enum HomeItemType { Desk, Chair, Table, Sofa, Bed }
    private HomeItemType itemType;

    public FurnitureItem(String price, String description, String itemType){
        super(price,description); // DataInvalidException
        setItemType(itemType); //DataInvalidException 
        totalCreated++;
    }

    public HomeItemType getItemType() {
        return itemType;
    }

    public void setItemType(String itemType){
        try {
            this.itemType = HomeItemType.valueOf(itemType);
        } catch (IllegalArgumentException ex){
            throw new DataInvalidException("Item type is invalid: " + itemType);
        }
    }
    
    @Override
    public String toDataString(){
        return String.join(";","Furniture",String.valueOf(getPrice()),getDescription(),itemType.toString());
    }
    @Override
    public String toString(){
        return String.format("Furniture name :%s, Serial number: %d, Price: %.2f, Description: %s", itemType,getSerialNo(),getPrice(),getDescription());
    }
}
