//abstract class HomeItem , interface Marshallable
package quiz3furniture;

public abstract class HomeItem implements Marshallable {
  private String description; // 2-50 characters except semicolon ; ` @ &
  private double price; // 0-999999 floating point, display with 2 points precision
  private int serialNo;
  static int totalCreated = 0;
  
    HomeItem() {
        setSerialNo(totalCreated+1);
    }
    HomeItem(String price, String description){
        this();
        setPrice(price); //DataInvalidException
        setDescription(description); //DataInvalidException
    }
  
    static HomeItem createFromDataLine(String line) {
        String[] data = line.split(";");
        if(data.length != 4 ){
            throw new DataInvalidException("Wrong format line: "+line);
        }
        
        switch(data[0]){
            case"Furniture" :
                return new FurnitureItem(data[1],data[2],data[3]);//DataInvalidException: data[1](price) , IllegalArgumentException : data[3] enum
            case "Electronics" :
                return new ElectronicsItem(data[1],data[2],data[3]);
            default :
                throw new DataInvalidException("Wrong Item name: "+data[0]);
        }
    
    }
    public void setSerialNo(int serialNo){
        this.serialNo = serialNo;
    }
    public int getSerialNo(){
        return serialNo;
    }
    static int getTotalCreated(){
        return totalCreated;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description){
        if(!description.matches("[^`@&]+") || description.length()<2 ||description.length()>50){
            throw new DataInvalidException("Description is invalid: "+ description);
        }
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(String price){
        try{
            double priceNum = Double.parseDouble(price);
            if(priceNum < 0 || priceNum > 999999){
                throw new DataInvalidException("Price has to be 0~999999: "+price);
            }
            this.price = priceNum;
            
        }catch (NumberFormatException ex){
            throw new DataInvalidException("Wrong Item price: "+price);
        }

    }
}

interface Marshallable{
    String toDataString();
}