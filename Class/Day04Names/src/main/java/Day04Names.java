/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author 1898918
 */
import java.util.Scanner;
public class Day04Names {
    public static void main(String[] args) {
        String[] names;
        int size;
        
        Scanner input = new Scanner(System.in);
        
        System.out.print("How many names do you want to enter? ");
        size = input.nextInt();
        input.nextLine();
        
        names = new String[size];
        for (int i = 0; i < size; i++) {
            System.out.print("Enter name #" + (i+1) + ": ");
            names[i] = input.nextLine();
        }
        
        System.out.print("Your names were: ");
        
        // String listStr =  String.join(",", names);
        // System.our.println(listStr);
        
        for (int i = 0; i < size; i++) {
            System.out.print(names[i] + ",");
        }
        System.out.printf("\b");   
    }
}
