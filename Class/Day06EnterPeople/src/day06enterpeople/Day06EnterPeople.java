/*
In a loop keep asking user for name and age of person and keep adding them to the list until user enters an empty name.

Ask user for search string.

After user is done entering names/ages compute and display the following.
* Average age of all people (floating point with 2 decimal points precision)
* Youngest person's name and age
* Name and age of person with the shortest name
* Names and ages of all persons who's names contain the search string.
 */
package day06enterpeople;

import java.util.Scanner;
import java.util.ArrayList;

public class Day06EnterPeople {
    
    static Scanner input = new Scanner(System.in);
	static ArrayList<Person> peopleList = new ArrayList<>();

    public static void main(String[] args) {

		while(true){
			System.out.printf("Enter person's name (empty to finish): ");
			String name = input.nextLine();
			if(name.isEmpty()){
				break;
			}

			System.out.printf("Enter age: ");
			int age = input.nextInt();
			input.nextLine();
			peopleList.add(new Person(name, age));
		}
		System.out.println();

		System.out.printf("Enter search string: ");
		String substring = input.nextLine();
		
		System.out.printf("Average age is: %.2f\n",averageOfAge());
		System.out.printf("Youngest person is: %s %d y/o\n",getYoungest().name,getYoungest().age);
		System.out.printf("Person with shortest name is: %s %d y/o\n",getShortest().name,getShortest().age);

		for(Person person : peopleList){
			if(person.name.contains(substring)){
				System.out.printf("Person with matching name: %s %d y/o\n",
					person.name, person.age);
			}
		}
			

    }
	static Person getShortest(){

		int shortest = peopleList.get(0).name.length();
		int index = 0;
		for(int i=1; i < peopleList.size(); i++){
			int length = peopleList.get(i).name.length();
			if(length < shortest){
				shortest = length;
				index = i;
			}
		}
		return peopleList.get(index);
	}

	static double averageOfAge(){
		int sum = 0;
		for(Person person : peopleList){
			sum += person.age;
		}

		return (double)sum/peopleList.size();
	}

	static Person getYoungest(){
		
		int youngest = peopleList.get(0).age;
		int index = 0;
		for(int i=1; i < peopleList.size(); i++){
			int age = peopleList.get(i).age;
			if ( age < youngest){
				youngest = age;
				index = i;
			}
		}
		return peopleList.get(index);
	}
    
}

class Person {
    public String name;
    public int age;

    
    public Person(String name, int age){
        this.name = name;
        this.age = age;
    }
    
    public void print(){
        System.out.printf("%s is %d y/o\n",name,age);
    }
}
