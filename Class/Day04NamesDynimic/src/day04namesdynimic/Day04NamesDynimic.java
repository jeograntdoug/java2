package day04namesdynimic;

import java.util.Scanner;
import java.util.ArrayList;

public class Day04NamesDynimic {

    public static void main(String[] args) {
        
        int size;
        
        Scanner input = new Scanner(System.in);
        
        System.out.print("How many names do you want to enter? ");
        size = input.nextInt();
        input.nextLine();
        
        ArrayList<String> namesList = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            System.out.print("Enter name #" + (i+1) + ": ");
            String str = input.nextLine();
            namesList.add(str);
        }
        
        System.out.print("Your names were: ");
        
        // String listStr =  String.join(",", names);
        // System.our.println(listStr);
        
        for (int i = 0; i < namesList.size(); i++) {
            System.out.printf("%s%s",i == 0 ? "" : ", " ,namesList.get(i));
        }
        
    }
    
}
