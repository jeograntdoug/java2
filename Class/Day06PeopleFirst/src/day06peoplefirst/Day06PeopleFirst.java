/*
    **PART A
    Find the youngest and oldest person and display their name and age.
    **PART B
    read in the contents of the file, parse it to create objects with provided names and ages.
    **PART C
    handling the necessary exceptions in case a line contains invalid information, such as:
    - name empty
    - age not within 0-150 range
    - invalid number of semicolon-separated fields in line, e.g. J;;;56

 */
package day06peoplefirst;

import java.util.ArrayList;
import java.io.File;
import java.io.PrintWriter;
import java.io.IOException;
import java.util.Scanner;

public class Day06PeopleFirst {

    static ArrayList<Person> peopleList = new ArrayList<>();
    static String fileName = "people.txt";
    public static void main(String[] args) {
        
/*     
        // PART A
        peopleList.add(new Person("Jerry",33));
        peopleList.add(new Person("Maria",22));
        peopleList.add(new Person("Theresa",54));
        peopleList.add(new Person("Tom",45));
        
        for(Person person : peopleList){
            person.print();
        }
       

        try(PrintWriter fileOutput = new PrintWriter(new File(fileName))){
            for(Person person : peopleList){
                fileOutput.printf("%s;%d\n",person.name,person.age);
            }
        }catch(IOException ex){
            System.out.println("Cannot open file:"+fileName);
        }
         //End of PART A
*/

        // PART B & PART C
        System.out.println("===========");
        System.out.println("Part B&C:");
        try(Scanner fileInput = new Scanner(new File(fileName))){
            while(fileInput.hasNextLine()){
                String line = fileInput.nextLine();
                String[] elements = line.split(";");
                
                try{
                    String name = elements[0];
                    if(name.isEmpty()){
                        throw new Exception("Name is Empty");
                    }
                    int age = Integer.parseInt(elements[1]);
                    if(age>150 || age<0){
                        throw new Exception("Age is not valid");
                    }   
                    
                    System.out.printf("%s is %d y/o\n",name,age); 
                } catch ( NumberFormatException ex) {
                    System.out.println("ParceInt Error : "+ex.getMessage());
                } catch (Exception ex){
                    System.out.println(ex.getMessage());
                }
                
 
            }
        } catch ( IOException ex ){
            System.out.println("Cannot open file:"+fileName);
        }
        // End of PART B & PART C
     
    }
    
}

class Person {
    public String name;
    public int age;

    
    public Person(String name, int age){
        this.name = name;
        this.age = age;
    }
    
    public void print(){
        System.out.printf("%s is %d y/o\n",name,age);
    }
}