
package day05readwritefirst;

import java.util.Scanner;
import java.io.PrintWriter;
import java.io.File;
import java.io.IOException;

public class Day05ReadWriteFirst {
    static Scanner input = new Scanner(System.in);
  
    public static void main (String[] args) throws IOException {

        String fileName = "file.txt";
        File file = new File(fileName);    

        try{
            writeInFile(file);
        }
        catch (IOException ex){
            System.out.println(ex.getMessage());
        }
        
        try{
            readAndPrintFromFile(file);
        }
        catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
        
    }

      static void writeInFile(File file) throws IOException {
        String myLine;
        int lineCount = 3;
        
        if(file.exists()){
            throw new IOException("File already exists!");
        }
        
        System.out.print("Enter a line of text:");
        myLine = input.nextLine();
        
        try( PrintWriter output = new PrintWriter(file))
        {
            for (int i = 0; i < lineCount; i++) {
                output.println(myLine);
            }
        }
        catch (IOException ex) {
            System.out.println("Cannot write in the file!");
            System.out.println(ex.getMessage());
        }
    }
    static void readAndPrintFromFile(File file) throws IOException {
        if(!file.exists()){
            throw new IOException("File doesn't exists");
        }
        
        try(Scanner scanFile = new Scanner(file))
        {
            while(scanFile.hasNext()){
                String scanStr = scanFile.nextLine();
                System.out.println(scanStr);
            }
        }
        catch (IOException ex) {
            System.out.println("Cannot read the file!");
            System.out.println(ex.getMessage());
        }
    }
    
}
