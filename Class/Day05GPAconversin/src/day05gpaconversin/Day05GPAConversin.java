package day05gpaconversin;

import java.util.Scanner;
import java.io.PrintWriter;
import java.io.IOException;
import java.io.File;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Collections;

public class Day05GPAConversin {

    public static void main(String[] args) throws IOException{
        String fileName = "grades.txt";
        ArrayList<Grade> gradeList = new ArrayList<>();
        
        File file = new File(fileName);
        if(!file.exists()){
            System.out.println("File doesn't exist!");
            return;
        }
        
        try(Scanner fileInput = new Scanner(file)){
            while(fileInput.hasNextLine()){
                String gradeStr = fileInput.nextLine();
                Grade grade = new Grade(gradeStr);
                gradeList.add(grade);
            }
        }
        catch (IOException ex) {
            System.out.println("Error :" + ex.getMessage());
        }
        
        System.out.println("");
        System.out.println("Grades List: ");
        boolean isFirst = true;
        for(Grade grade : gradeList){
            System.out.printf("%s%s",isFirst?"":", ",grade.getGradeStr());
            isFirst = false;
        }
        System.out.println("");
        
        isFirst = true;
        for(Grade grade : gradeList){
            System.out.printf("%s%.1f",isFirst?"":", ",grade.getGradeNum());
            isFirst = false;
        }
        System.out.println("");
        System.out.printf("Your Average : %.2f\n" , getAverage(gradeList));
        System.out.printf("Your Median : %.2f\n", getMedian(gradeList));
        System.out.printf("Your Standard Deviation : %.2f\n", getStandardDeviation(gradeList));
    }
    
    static double getAverage(ArrayList<Grade> gradeList){
        double sum = 0;
        for(Grade grade : gradeList){
            sum += grade.getGradeNum();
        }
        return sum/gradeList.size();
    }

    static double getMedian(ArrayList<Grade> gradeList){
        Collections.sort(gradeList,new GradeComparator());
        double median;
        if(gradeList.size() % 2 == 1){
            median = gradeList.get((int)gradeList.size()/2).getGradeNum();
        } else {
            double v1 = gradeList.get((int)gradeList.size()/2).getGradeNum();
            double v2 = gradeList.get((int)gradeList.size()/2 -1).getGradeNum();
            median = (v1+v2)/2;
        }
        
        return median;
    }
    static double getStandardDeviation(ArrayList<Grade> gradeList){
        double average = getAverage(gradeList);
        double sum = 0;
        for(Grade grade : gradeList){
            sum += Math.pow((grade.getGradeNum() - average) , 2);
        }
        
        return Math.sqrt(sum/gradeList.size());
    }
}

class Grade {
    private final double gradeNum;
    private final String gradeStr;
    
    Grade (String gradeStr){
        this.gradeStr = gradeStr;
        this.gradeNum = convertGradeToNum(gradeStr);
    }
    
    double convertGradeToNum(String grade){
        if(grade.equals("A")){
            return 4.0;
        } else if(grade.equals("A-")){
            return 3.7;
        } else if(grade.equals("B+")){
            return 3.3;
        } else if(grade.equals("B")){
            return 3.0;
        } else if(grade.equals("B-")){
            return 2.7;
        } else if(grade.equals("C+")){
            return 2.3;
        } else if(grade.equals("C")){
            return 2.0;
        } else if(grade.equals("C-")){
            return 1.7;
        } else if(grade.equals("D+")){
            return 1.3;
        } else if(grade.equals("D")){
            return 1.0;
        } else if(grade.equals("D-")){
            return 0.7;
        } else if(grade.equals("F")){
            return 0;
        } else {
            return -1;
        }
    }
    
    double getGradeNum(){
        return gradeNum;
    }
    String getGradeStr(){
        return gradeStr;
    }
    
    public int compareTo(Grade grade){
        if(this.gradeNum < grade.getGradeNum()){
            return -1;
        } else if(this.gradeNum > grade.getGradeNum()) {
            return 1;
        } else{
            return 0;
        }
    }
}

class GradeComparator implements Comparator<Grade> {
    public int compare(Grade grade1, Grade grade2) {
        return grade1.compareTo(grade2);
    }
}