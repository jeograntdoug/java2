/*
Day04Numbers
------------

Ask user how many numbers he/she wants to generate.
Generate the numbers as random integers in -100 to +100 range, both inclusive.
Place the numbers in ArrayList<Integer> named numsList.

In the next loop find the numbers that are less or equal to 0 and print them out, one per line.
*/
package day04numbers;

import java.util.ArrayList;
import java.util.Scanner;


public class Day04Numbers {


    public static void main(String[] args) {
                int size;
        int random;
        Scanner input = new Scanner(System.in);
        
        ArrayList<Integer> numsList = new ArrayList<>();
        
        System.out.print("How many numbers you wants to generate? ");
        size = input.nextInt();
        input.nextLine();
        
        // Add random number(-100<=number , 100>=number)
        for(int i=0; i<size ; i++) {
            random = (int)(201*Math.random()) - 100;
            numsList.add(random);
        }
        
        // Print list members
        for (int i = 0; i < numsList.size(); i++) {
            if(numsList.get(i)<= 0){
                System.out.println(numsList.get(i));
            }
        }

        
    }
    
}
