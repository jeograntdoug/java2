package day08peopleagain;

import java.util.ArrayList;

class Person {

    public Person(String name, int age) {
        setName(name);
        setAge(age);
    }

    private String name;
    private int age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if( name.length() <2 || name.length()> 50){
            throw new IllegalArgumentException("Name must be between 2-50 characters long");
        }
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        if(age < 0 || age > 150){
            throw new IllegalArgumentException("Age must be between 1-150");
        }
        this.age = age;
    }
    
    @Override
    public String toString() {
        return String.format("%s is %d y/o ",getName(),getAge());
    }
}

class Student extends Person {
    private String program; // length 2-50
    private double gpa; // 0.0-4.0

    public Student(String name, int age, String program, double gpa) {
        super(name, age);
        setProgram(program);
        setGpa(gpa);
    }

    public double getGpa() {
        return gpa;
    }

    public void setGpa(double gpa) {
        if(gpa < 0.0 || gpa >4.0){
            throw new IllegalArgumentException("Name must be between 2-50 characters long");
        }
        this.gpa = gpa;
    }

    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        if(program.length() < 2 || program.length() >50){
            throw new IllegalArgumentException("Name must be between 2-50 characters long");
        }
        this.program = program;
    }
    
    @Override
    public String toString(){
        return super.toString() + String.format("/ %s program, %.2f gpa",
                        getProgram(),getGpa());
    }
}
class Teacher extends Person {
    String subject;
    int yearsOfExperience;

    public Teacher(String name, int age, String subject, int yearsOfExperience) {
        super(name, age);
        setSubject(subject);
        setYearsOfExperience(yearsOfExperience);
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        if(subject.length() < 2 || subject.length() >50){
            throw new IllegalArgumentException("Name must be between 2-50 characters long");
        }
        this.subject = subject;
    }

    public int getYearsOfExperience() {
        return yearsOfExperience;
    }

    public void setYearsOfExperience(int yearsOfExperience) {
        if(yearsOfExperience < 0 || yearsOfExperience >100){
            throw new IllegalArgumentException("Name must be between 2-50 characters long");
        }
        this.yearsOfExperience = yearsOfExperience;
    }
    
    @Override
    public String toString(){
        return super.toString() + String.format("/ %s subject, %d years of experience",
                        getSubject(),getYearsOfExperience());
    }
}
public class Day08PeopleAgain {
    static ArrayList<Person> peopleList = new ArrayList<>();
    
    public static void main(String[] args) {
        for (int i = 0; i < 2; i++) {
            peopleList.add(new Person("Person"+i,(int)(Math.random()*100 +1) ));
            peopleList.add(new Student("Student"+i,(int)(Math.random()*100 +1), "math_"+i, Math.random()*4 ));
            int randomAge = (int)(Math.random()*100 +1);
            peopleList.add(new Teacher("Teacher"+i,randomAge, "Humanity_"+i, (int)(Math.random()*randomAge) ));
        }
        
        for(Person p : peopleList){
            System.out.printf("%s is %d y/o ",p.getName(),p.getAge());
            
            if(p instanceof Student){
                System.out.printf("/ %s program, %.2f gpa\n",
                        ((Student)p).getProgram(),((Student)p).getGpa());
            } else if(p instanceof Teacher){
                System.out.printf("/ %s subject, %d years of experience\n",
                        ((Teacher)p).getSubject(),((Teacher)p).getYearsOfExperience());
            } else {
                System.out.printf("\n");
            }
        }
    }
    
    
}
