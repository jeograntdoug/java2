package day08todos;

import java.text.ParseException;
import java.util.InputMismatchException;
import java.io.IOException;

import java.io.File;
import java.io.PrintWriter;
import java.util.Scanner;

import java.util.ArrayList;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class Day08Todos {

    static ArrayList<Todo> todoList = new ArrayList<>();
    static Scanner input = new Scanner(System.in);
    static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

    public static void main(String[] args){

        loadDataFromFile();

        while(true){
            int choice = getMenuChoice();

            switch(choice){
                case 1 : 
                        addTodo();
                        break;
                case 2 : 
                        printAll();
                        break;
                case 3 :
                        removeTodo();
                        break;
                case 4 :
                        modifyTodo();
                        break;
                case 0 :
                    try{
                            saveDataToFile();
                            System.out.println("Existing. Good bye!");
                            return;
                    } catch (IOException ex){
                            System.out.println("Cannot write in the file");
                    }
                default :
                    System.out.println("Wrong choice");
            }
            System.out.println();
        }
    }

    static int getMenuChoice(){

        while(true){
            System.out.print(
                            "Please make a choice [0-4]:"
                            +"\n1. Add a todo"
                            +"\n2. List all todos (numbered)"
                            +"\n3. Delete a todo"
                            +"\n4. Modify a todo"
                            +"\n0. Exit"
                            +"\nYour choice is:"
            );

            int choice = inputInt();

            if(todoList.size() ==0 && choice >1 && choice <= 4){
                System.out.println("\nThere are no todos\n");
                continue;
            }
            return choice;
        }

    }

    static void addTodo(){
        try{
            System.out.println("Adding a todo.");
            
            System.out.print("Enter task description: ");
            String task = input.nextLine();

            System.out.print("Enter due Date (yyyy/mm/dd): ");
            String dateStr = input.nextLine();
            Date date = dateFormat.parse(dateStr);

            System.out.print("Enter hours of work (integer): ");
            int hoursOfWork = inputInt();
      
            Todo newTodo = new Todo(task,date,hoursOfWork,"Pending");
            todoList.add(newTodo);
            
            System.out.println("You've created the following todo:");
            System.out.println(newTodo);

        }catch (ParseException ex){
            System.out.println("Date is wrong format");

        }catch (IllegalArgumentException ex){
            System.out.println(ex.getMessage());
			
        }
    }
    
    static void printAll(){
        System.out.println("Listing all todos.");
        if(todoList.size() == 0){
            System.out.println("There are no todos");
        }
        for(int i=0 ; i < todoList.size() ; i++){
            System.out.printf("#%d: %s\n",i+1,todoList.get(i));
        }
    }
    
    static void modifyTodo(){
        try {
            System.out.print("Modifying a todo. Which todo # would you like to modify? ");
            int numTodo = inputInt();

            if(numTodo < 1 || numTodo > todoList.size()){
                throw new IllegalArgumentException(String.format("There is no #%d todo in the list",numTodo));
            }
            
            Todo todo = todoList.get(numTodo-1);
            System.out.printf("Modifying todo #%d %s\n",numTodo,todo);

            System.out.print("Enter task description: ");
            String task = input.nextLine();

            System.out.print("Enter due Date (yyyy/mm/dd): ");
            String dateStr = input.nextLine();
            Date date = dateFormat.parse(dateStr);

            System.out.print("Enter hours of work (integer): ");
            int hoursOfWork = inputInt();
                        
            System.out.print("Enter status of work (Pending or Done): ");
            String status = input.nextLine();
            
            Todo newTodo = new Todo(task,date,hoursOfWork,status);
            todoList.set(numTodo, newTodo);

        }catch (ParseException ex){
            System.out.println("Date is wrong format");
        }catch (IllegalArgumentException ex){
            System.out.println(ex.getMessage());
        }
    }

    static void removeTodo(){
        try{
            System.out.print("Deleting a todo. Which todo # would you like to delete? ");
            int numTodo = inputInt();

            if(numTodo<1 || numTodo > todoList.size()){
                throw new IllegalArgumentException(String.format("There is no #%d todo in the list",numTodo));
            }

            todoList.remove(numTodo-1);

            System.out.println("Deleted todo #1 successfully.");

        }catch (IllegalArgumentException ex){
            System.out.println(ex.getMessage());
        }
    }
    
    static final String TEXT_FILE = "todos.txt";

    static void loadDataFromFile() {
        try(Scanner fileInput = new Scanner(new File(TEXT_FILE))){
            while(fileInput.hasNextLine()){
                
                try{
                    String line = fileInput.nextLine();
                    todoList.add(new Todo(line));
                } catch (IllegalArgumentException | ParseException ex){
                    System.out.println("Error: "+ex.getMessage());
                }
                
            }
        } catch (IOException ex){
            System.out.println("Cannot read the file");
        }
    }
    
    static void saveDataToFile() throws IOException {
        try(PrintWriter fileOutput = new PrintWriter(new File(TEXT_FILE))){
            for(Todo todo : todoList){
                fileOutput.println(todo.toDataString());
            }
        } 
    }
    
    static int inputInt() {
        while(true){
            try{
                int num = input.nextInt();
                input.nextLine();
                return num;
            } catch (InputMismatchException ex) {
                System.out.print("Invalid integer, please try again: ");
                input.nextLine();
            }
            
            //This is fine too.
            //finally{} is excuted no matter how you escape the try{} part
            /*
                try{
                int num = input.nextInt();
                return num;
            } catch (InputMismatchException ex) {
                System.out.print("Invalid integer, please try again: ");
            } finally {
                input.nextLine();
            }
            */
        }
    }
}

class Todo {
    enum TaskStatus { Pending, Done };
    
    private String task;
    private Date dueDate;
    private int hoursOfWork;
    private TaskStatus status;
    
    private SimpleDateFormat dateFormatScreen = new SimpleDateFormat("yyyy/MM/dd");
    private SimpleDateFormat dateFormatFile = new SimpleDateFormat("yyyy-MM-dd");
    
    private Pattern patternTask = Pattern.compile("[;|`]");
    
    public Todo(String task, Date date, int hoursOfWork, String statusStr) throws ParseException{
        setTask(task);
        setDueDate(date);
        setHoursOfWork(hoursOfWork);
        setTaskStatus(statusStr);
    }

    public Todo(String dataLine) throws ParseException {
        String[] data = dataLine.split(";");
        if(data.length != 4){
                throw new IllegalArgumentException("File Data is worng format :"+dataLine);
        }
        setTask(data[0]);
        setDueDate(dateFormatFile.parse(data[1]));
        setHoursOfWork(Integer.parseInt(data[2]));
        setTaskStatus(data[3]);
    }

    public void setTask(String task){
        if(task.length() <2 || task.length() >50){
            throw new IllegalArgumentException("Task must be 2-50 characters long");
        }

        if(patternTask.matcher(task).find()){
            throw new IllegalArgumentException("Task must exclude ` | ; ");
        }

        this.task = task;
    }
    public void setDueDate(Date dueDate) throws ParseException{

        Calendar cal = Calendar.getInstance();
        cal.setTime(dueDate);
        
        Date dateFrom= dateFormatScreen.parse("1900/1/1");
        Date dateTo= dateFormatScreen.parse("2100/12/31");
        
        if(dueDate.before(dateFrom) || dueDate.after(dateTo)){
            throw new IllegalArgumentException("Date must be 1900 - 2100");
        }

        this.dueDate = dueDate;

    }

    public void setHoursOfWork(int hoursOfWork){
        if(hoursOfWork<0){
            throw new IllegalArgumentException("Hours of work has to be greater than 0");
        }
        this.hoursOfWork = hoursOfWork;
    }
    
    public void setTaskStatus(String statusStr){
        TaskStatus status = TaskStatus.valueOf(statusStr);

        this.status = status;
    }

    public String getTask(){
        return task;
    }
    public Date getDueDate(){
        return dueDate;
    }
    public int getHoursOfWork(){
        return hoursOfWork;
    }
    public String getTaskStatus(){
        return status.toString();
    }

    public String toDataString() {
        return String.join(";",String.valueOf(task),
            String.valueOf(dateFormatFile.format(dueDate)),
            String.valueOf(hoursOfWork),
            String.valueOf(status));
    }

    @Override
    public String toString(){ 
        return String.format("%s, %s, will take %d hour(s) of work: %s",task, dateFormatScreen.format(dueDate),hoursOfWork,status);
    }
}


//Question

//1.Model has to handle exception? or just throw it, controller handles. 
//2.nextInt is Pain in the ass. must take care of buffer with nextLine() when exception happen
//3.FIXME : right form, Wrong number of date works / No exception

//Answer
//1.model just Throw exception
//2.
//3.Design like this way. Use Gui

//Good to know
//finally{} is excuted no matter how you escape the try{} part (line : 222) in inputInt() method