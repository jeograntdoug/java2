package day05maxmin;

import java.util.Scanner;
import java.util.ArrayList;

public class Day05MaxMin {

    static Scanner input = new Scanner(System.in);
    static ArrayList<Double> numList = new ArrayList<>();
    
    public static void main(String[] args) {
        while(true){
            System.out.print("Enter a positive number( exit : 0): ");
            double num = input.nextDouble();
            if(num == 0){
                break;
            }
            numList.add(num);  
            input.nextLine();
        }
        System.out.println("");
        
        System.out.printf("Sum of all numbers is: %.2f\n",getSumOfNums());
        System.out.printf("Average of all numbers is: %.2f\n",getAverageOfNums());
        System.out.printf("Maximum: %.2f\n",getMaximum());
        System.out.printf("Minimum: %.2f\n",getMinimum());
        
    }
    
    static double getSumOfNums() {
        double sum=0;
        for(double num : numList){
            sum += num;
        }
        
        return sum;
    }
    static double getAverageOfNums(){
        return getSumOfNums()/numList.size();
    }
    static double getMaximum(){
        return java.util.Collections.max(numList);
    }
    static double getMinimum(){
        return java.util.Collections.min(numList);
    }
    
}
