
package day05namesfromfile;

import java.util.Scanner;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class Day05NamesFromFile {

    static Scanner input = new Scanner(System.in);
    
    public static void main(String[] args) throws IOException{
        File namesFile = new File("names.txt");
        ArrayList<String> namesList = new ArrayList<>();
        
        if(!namesFile.exists()) {
            System.out.println("File doesn't exists!");
            return;
        }
        
        try(Scanner fileInput = new Scanner(namesFile)){
            while(fileInput.hasNextLine()){
                namesList.add(fileInput.nextLine());
            }
        }
        catch (IOException ex) {
            System.out.println("Error : "+ ex.getMessage());
        }
        
        System.out.println("Names read from file:");
        boolean isFirst = true;
        for(String name : namesList) {
            System.out.printf("%s%s",isFirst?"":", ",name);
            isFirst = false;
        }
        System.out.println("");
        System.out.println("");
        
        System.out.print("Enter search string:");
        String search = input.nextLine();
        System.out.println("");
        
        ArrayList<String> searchList = searchStr(namesList, search);
        
        for(String searchName : searchList){
            System.out.print("Matching name: "+searchName);
            System.out.println("");
        }
        System.out.println("");
        
        System.out.println("Longest name is: "+findLongest(namesList));

        
    }
    
    static ArrayList<String> searchStr(ArrayList<String> namesList, String Search){
        ArrayList<String> searchList = new ArrayList<>();
        try(PrintWriter foundOutput = new PrintWriter(new File("found.txt"))){
            for( String name : namesList){
                if(name.contains(Search)){
                    searchList.add(name);
                    foundOutput.println(name);
                }
            }
        }
        catch (IOException ex){
            System.out.println("Error: "+ex.getMessage());
        }
        return searchList;
    }
    static String findLongest(ArrayList<String> namesList){
        int longestLength=0;
        String longestName = "";
        
        try( PrintWriter longestOutput = new PrintWriter(new File("longest.txt"))){
            for(String name : namesList){
                int length = name.length();
                if(length>longestLength){
                    longestName = name;
                    longestLength = length;
                }
            }
            longestOutput.println(longestName);
        }
        catch (IOException ex) {
            System.out.println("Error: " +ex.getMessage());
        }
        return longestName;
    }
}
