package quiz2travel;

import java.io.File;
import java.io.IOException;
import java.security.InvalidParameterException;
import java.util.InputMismatchException;
import java.text.ParseException;

import java.util.Scanner;
import java.util.ArrayList;

import java.util.Date;
import java.text.SimpleDateFormat;


public class Quiz2Travel {

    static ArrayList<Trip> travelList = new ArrayList<>();
    static Scanner input = new Scanner(System.in);
    static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public static void main(String[] args) {
        
        readDataFromFile();
        
        boolean flag = true;
        while(flag){
            try{
                System.out.printf("\n1. Display list of all travel plans"
                        + "\n2. Add new Trip"
                        + "\n3. Find and display all trips that have not departed yet but will in the future"
                        + "\n4. Find and display the trip of the longest duration"
                        + "\n0. Exit."
                        + "\nPlease choose an action: ");
                int menu = input.nextInt();
                input.nextLine();
                System.out.println("");
            
                switch(menu){
					//1. Display list of all travel plans using print()
                    case 1 :
                        System.out.println("Print all travel plans");
                        for (Trip trip : travelList) {
                            trip.print();
                        }
                        break;

					//2. Add new Trip to the list from user input
                    case 2:
                        try {
                            System.out.println("Add new Trip");
                            System.out.print("Enter name: ");
                            String name = input.nextLine();
                            if (name.length() < 1 || name.length() > 50) {
                                throw new InvalidParameterException("name must be between 1-50 characters long: \n" + name);
                            }

                            System.out.print("Enter city: ");
                            String city = input.nextLine();
                            if (city.length() < 1 || city.length() > 50) {
                                throw new InvalidParameterException("city must be between 1-50 characters long: \n" + city);
                            }

                            System.out.print("Enter departure day(yyyy-MM-dd)");
                            String departureDate = input.nextLine();

                            Date dateDeparture = dateFormat.parse(departureDate);

                            System.out.print("Enter return day(yyyy-MM-dd)");
                            String returnDate = input.nextLine();
                            Date dateReturn = dateFormat.parse(returnDate);

							if (dateReturn.getTime() < dateDeparture.getTime()) {
								throw new InvalidParameterException("Departure date must be before return date");
							}


                            Trip newTrip = new Trip(name, city, dateDeparture, dateReturn);
                            travelList.add(newTrip);
                            
                        } catch (InvalidParameterException ex) {
                            System.out.println(ex.getMessage());
                        } catch (ParseException ex) {
                            System.out.println("Wrong Date format! :"+ex.getMessage());
                        }
                        break;

					//3. Find and display all trips that have not departed yet but will in the future
                    case 3:
                        Date rightNow = new Date();
                        System.out.println("all trips that have not departed yet but will in the future: ");
                        for(Trip trip : travelList){
                            if(trip.departureDate.getTime() > rightNow.getTime()){
                                trip.print();
                            }
                        }
                        break;

					//4. Find and display the trip of the longest duration [(*) hard].
                    case 4:
                        Trip longest = travelList.get(0);
						long longestDuration = longest.returnDate.getTime() - longest.departureDate.getTime();
                        for(int i= 1; i < travelList.size(); i++) {
							Trip trip = travelList.get(i);

							long duration = trip.returnDate.getTime() - trip.departureDate.getTime();
                            if(duration > longestDuration){
                                longest = trip;
								longestDuration = duration;
                            }
                        }
                        System.out.println("Find and display the trip of the longest duration [(*) hard]: ");
                        longest.print();
                        break;

					//0. Exit.
                    case 0 :
                        flag = false;
                        break;
                    default : 
                        throw new InvalidParameterException();
                }
            } catch (InputMismatchException ex){
                input.nextLine();   // Need to empty buffer
                System.out.println("Wrong menu! Chose again!!");
            } catch (InvalidParameterException ex){
                System.out.println("Wrong menu! Chose again!!");  
            }
        }
    }

    static void readDataFromFile() {
        try ( Scanner fileInput = new Scanner(new File("travels.txt"))) {
            while (fileInput.hasNextLine()) {
                try {
                    String line = fileInput.nextLine();

                    String[] lineElements = line.split(";");
                    if (lineElements.length != 4) {
                        throw new InvalidParameterException("The number of fields must be exactly 4: \n" + line);
                    }

                    String name = lineElements[0];
                    String city = lineElements[1];

                    if (name.length() < 1 || name.length() > 50
                            || city.length() < 1 || city.length() > 50) {
                        throw new InvalidParameterException("name and city must be between 1-50 characters long: \n" + line);
                    }

                    Date departureDate = dateFormat.parse(lineElements[2]);
                    Date returnDate = dateFormat.parse(lineElements[3]);

                    if (returnDate.getTime() < departureDate.getTime()) {
                        throw new InvalidParameterException("Departure date must be before return date: \n" + line);
                    }

                    Trip trip = new Trip(name, city, departureDate, returnDate);
                    travelList.add(trip);

                } catch (InvalidParameterException | ParseException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        } catch (IOException ex) {
            System.out.println("File cannot be read at all!!!!!\nByeBye");
            System.exit(1);
        }
    }
}

class Trip {

    String travellerName, cityToVisit;
    Date departureDate, returnDate;
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    public Trip(String travellerName, String cityToVisit, Date departureDate, Date returnDate) {
        this.travellerName = travellerName;
        this.cityToVisit = cityToVisit;
        this.departureDate = departureDate;
        this.returnDate = returnDate;
    }

    public void print() {
        System.out.printf("%s travles to %s on %s and return %s\n",
                travellerName, cityToVisit, dateFormat.format(departureDate), dateFormat.format(returnDate));
    }
    
}