/*
Day04NamesPlus
--------------

Create new project in Netbeans called Day04NamesPlus.

Decare ArrayList<Strings> as nameList.

In a loop ask user for a name.
If name is not empty add name to the list and continue to next iteration of the loop.
If name is empty - exit the loop.

After loop is done print out all the names, semicolon-separated on a single line.
*/
package day04namesplus;

import java.util.ArrayList;
import java.util.Scanner;
public class Day04namesPlus {
   
    public static void main(String[] args) {
       ArrayList<String> namesList = new ArrayList<>();
       String str;
       Scanner input = new Scanner(System.in);
       
        while(true){
            System.out.print("Enter names :");
            str = input.nextLine();
            
            if(str.isEmpty()){
                break;
            } else {
                namesList.add(str);
            }
        }
        
        // Print names
        for (int i = 0; i < namesList.size(); i++) {
            System.out.printf("%s%s", i==0?"":":",namesList.get(i));
        }
    }
}
