import java.util.*;


class Test {
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		int myInt;
		boolean continueInput = true;
		System.out.println(Long.MAX_VALUE);
		System.out.println(Long.MAX_VALUE*2);
		double i = 1.0/0;
		System.out.println(i);
		long value = Long.MAX_VALUE + 1;

		do {
			try{
				System.out.print("Enter an integer: ");
				myInt = input.nextInt();

				System.out.println("The number entered is "+ myInt);

				continueInput = false;
			}
			catch(InputMismatchException ex){
				System.out.println("It's not an integer");
				input.nextLine();
			}
		} while(continueInput);
		
	}
}
