import java.util.ArrayList;

public class Course{
	private String courseName;		
	private ArrayList<String> students;
	private int numberOfStudents;

	public Course(String courseName){
		this.courseName = courseName;
		students = new ArrayList<>();
		numberOfStudents = 0;
	}

	public String getCourseName() {
		return courseName;
	}
	public void addStudent(String student) {
		students.add(student);
		numberOfStudents++;
	}
	public void dropStudent(String student) {
		if(numberOfStudents > 0){
			students.remove(student);
			numberOfStudents--;
		} else {
			System.out.println("There is no student");
			return;
		}
	}

	public ArrayList<String> getStudents(){
		return students;
	}
	public int getNumberOfStudents() {
		return students.size();
	}
}
