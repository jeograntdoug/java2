import java.util.ArrayList;

class TestCourse {
	public static void main(String[] args){
		ArrayList<String> studentsList;

		Course course1 = new Course("Data Structures");
		Course course2 = new Course("Java2");

		String[] studentsName = {"Peter Jones","Kim Smith","Anne Kennedy"};

		for(String student: studentsName){
			course1.addStudent(student);
			course2.addStudent(student);
		}

		System.out.printf("Number of students in course1: %d\n",course1.getNumberOfStudents());
		studentsList = course1.getStudents();
		for(int i=0; i < studentsName.length; i++){
			System.out.printf("%s%s",i==0?"":"," , studentsList.get(i));
		}
		System.out.println();

		course2.dropStudent(studentsName[0]);
		System.out.printf("Number of students in course2: %d\n",course2.getNumberOfStudents());

		studentsList = course2.getStudents();
		for(int i=0; i < course2.getNumberOfStudents(); i++){
			System.out.printf("%s%s",i==0?"":"," , studentsList.get(i));
		}
		System.out.println();
	}

}
