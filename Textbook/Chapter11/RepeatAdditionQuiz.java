import java.util.Scanner;
import java.util.ArrayList;

class RepeatAdditionQuiz {
	public static void main(String[] args){
		final int RIGHT_ANS = 5+9;

		ArrayList<Integer> answerList = new ArrayList<>();
		Scanner input = new Scanner(System.in);
		int answer;

		while(true){

			System.out.printf("What is 5 + 9? ");
			answer = input.nextInt();
			input.nextLine();

			if(answer == RIGHT_ANS){
				System.out.println("You got it!");
				return;
			} else if (answerList.indexOf(answer) >= 0){
				System.out.printf("You already entered %d\n",answer);
			} else {
				answerList.add(answer);
			}
			System.out.printf("Wrong answer. Try Again. ");
		
		}
			
	}
}
