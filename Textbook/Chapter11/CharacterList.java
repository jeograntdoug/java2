import java.util.ArrayList;
import java.util.Arrays;

class CharacterList {
	public static void main(String[] args){
		String str = "HelloWorld!";

		System.out.println(str);
		System.out.println(toCharacterArray(str));
		
	}

	public static ArrayList<Character> toCharacterArray(String s){
		ArrayList<Character> list = new ArrayList<>();
		for(int i=0; i < s.length(); i++){
			list.add(s.charAt(i));
		}
		return list;
	}
}
