import java.util.ArrayList;
import java.util.Scanner;

public class CombineTwoLists {
	public static void main(String[] args){
		
		ArrayList<ArrayList<Integer>> list = new ArrayList<>();

		Scanner input = new Scanner(System.in);

		for(int j=0; j < 2; j++){
			System.out.printf("Enter 5 integers for list%d: ",(j+1));
			list.add(new ArrayList<Integer>());
			
			for(int i=0; i < 5; i++){
				list.get(j).add(input.nextInt());
			}
			input.nextLine();
		}

		// Merge two list
		ArrayList<Integer> listUnion = union(list.get(0),list.get(1));

		System.out.printf("The combined list is ");

		int size = listUnion.size();
		for(int i=0; i < size; i++){
			System.out.printf("%s%d",i==0?"":" ",listUnion.get(i));
		}
	}

	public static ArrayList<Integer> union (
			ArrayList<Integer> list1, ArrayList<Integer> list2)	{
		ArrayList<Integer> list = new ArrayList<>();

		int size = list1.size();

		for(int i=0; i < size; i++){
			int num = list1.get(i);
			list.add(num);
		}

		size = list2.size();

		for(int i=0; i < size; i++){
			int element = list2.get(i);
			list.add(element);
		}
		return list;
	}
	
}
