import java.util.Scanner;
import java.util.ArrayList;

//Find Perfect Square
class PerfectSquare {
	public static void main(String[] args){
		ArrayList<MyInteger> integerList = new ArrayList<>();
		Scanner input = new Scanner(System.in);
		int userNumber;

		System.out.printf("Enter an integer m: ");
		userNumber = input.nextInt();
		input.nextLine();

		int i = 2;
		int count = 0;

		int number = userNumber;
		// Loop until number : 1
		while(true){
			// loop if <i> can divide number
			while((number/i > 0) && (number%i == 0)){
				number /= i;
				count++;
			}

			// <i> cannot divide number anymore
			// push integer into list
			if(count != 0){
				MyInteger myInteger = new MyInteger(i,count);
				integerList.add(myInteger);
			}

			// no more division avaliable
			if (number == 1){
				break;
			}

			// new <i>, reset count
			count = 0;
			i++;
		}

		// smallest number to multiply to make perfect square
		int smallestNum = 1;
		for(MyInteger myInt : integerList){
			// only number has odd count, needed to multiply
			if(myInt.needCount()){
				smallestNum *= myInt.getMyInt();
			}
		}

		System.out.printf("The smallest number n for m * n to be a perfect square is %d",(smallestNum*userNumber));
		

	}
}
class MyInteger{
	private int myInt;
	private int count; 
	private boolean neededCount; 

	MyInteger (int myInt, int count){
		this.myInt = myInt;
		this.count = count;
		
		// only number has odd count, needed to multiply
		this.neededCount = (count%2 == 1);
	}

	public int getMyInt(){
		return myInt;
	}

	public boolean needCount(){
		return neededCount;
	}	
}
