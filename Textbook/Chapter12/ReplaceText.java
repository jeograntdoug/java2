import java.io.*;
import java.util.Scanner;

public class ReplaceText{
	public static void main(String[] args) throws Exception{
		// Check command line parameter usage
		if(args.length != 4){
			System.out.println("Usage: java ReplaceText sourceFile targetFile oldStr newStr");
			return;
		}
		String sourceFile = args[0];
		String targetFile = args[1];
		String oldString = args[2];
		String newString = args[3];

		// Check if source file exists
		File oldFile = new File(sourceFile);
		if(!oldFile.exists()){
			System.out.println("There is no file:"+sourceFile);
			return;
		}

		// Check if target file exists
		File newFile = new File(targetFile);
		if(newFile.exists()){
			System.out.println(targetFile+" already exists");
			return;
		}

		try (
			// Create input and output files
			Scanner input= new Scanner(oldFile);
			PrintWriter output= new PrintWriter(newFile);
		) {
			while (input.hasNext()){
				String s1 = input.nextLine();
				String s2 = s1.replaceAll(oldString,newString);
				output.println(s2);
			}
		}
	}
}
