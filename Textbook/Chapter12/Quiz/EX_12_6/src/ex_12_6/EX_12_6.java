/*
    NumberFormatException
 */

import java.util.Scanner;

public class EX_12_6 {

    static Scanner input = new Scanner(System.in);
    
    public static void main(String[] args) {
        System.out.println("Enter ");
        String str = input.nextLine();
        try{
            System.out.println(hex2Dec(str));
        } catch (HexFormatException ex){
            System.out.println("Error: " +ex.getMessage());
        }
    }
    
    static int hex2Dec (String HexString) {
		try{
			return Integer.parseInt(HexString,16);
		} catch (NumberFormatException ex){
			throw new HexFormatException("HexFormatException");
		}
    }
}

