import java.util.Scanner;

public class EX_12_1 {
	public static void main(String[] args){
		// If it's numeric use with isEmpty()
		if(args[0].matches("[-+]?\\d+\\.?\\d+")){
			System.out.println("Matches!!!");
		}

		try{
			int i =0;
			int num1 = Integer.valueOf(args[i]);
			String operator = args[++i];
			int num2 = Integer.valueOf(args[++i]);
			
			switch(operator){
				case "+" :
					System.out.printf("%d%s%d = %d",num1,operator,num2,num1+num2);
					break;
				case "-" :
					System.out.printf("%d%s%d = %d",num1,operator,num2,num1-num2);
					break;
				case "*" :
					System.out.printf("%d%s%d = %d",num1,operator,num2,num1*num2);
					break;
				case "/" :
					System.out.printf("%d%s%d = %d",num1,operator,num2,num1/num2);
					break;
				default :
					throw new Exception("Operator : " + operator);

			}
		}
		catch ( Exception ex) {
			System.out.println("Wrong Input: "+ex.getMessage());
		}
		System.out.println();
	}
	
}
