/*
    IllegalTriangleException
 */
package ex_12_5;

public class EX_12_5 {

    public static void main(String[] args) {
        try{
            Triangle tri = new Triangle(1.0,4.0,2.0);
            System.out.println("Succeed");
        } catch(IllegalTriangleException ex){
            System.out.println(ex.getMessage());
        }
    }
    
}

class Triangle {
    double side1,side2,side3;
    public Triangle(double side1, double side2, double side3) throws IllegalTriangleException {
        double sum = side1+side2+side3;
        if(sum/2 <= side1 || sum/2 <= side2 || sum/2 <= side3){
            throw new IllegalTriangleException("Cannot make triangle with these sides!");
        }
    }
}
class IllegalTriangleException extends Exception {
    public IllegalTriangleException(String str){
        super(str);
    }
}