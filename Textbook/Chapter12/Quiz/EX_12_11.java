import java.util.Scanner;
import java.util.ArrayList;
import java.io.PrintWriter;
import java.io.File;
import java.io.IOException;


public class EX_12_11 {
	Scanner input = new Scanner(System.in);

	public static void main(String[] args){

		if(args.length != 2){
			System.out.println("Arguments is not enough(requred 2)");
			return;
		}

		String str = args[0];
		String fileName = args[1];

		File file = new File(fileName);
		File newFile = new File("_"+fileName);

		try(Scanner fileInput = new Scanner(file);
				PrintWriter fileOutput = new PrintWriter(newFile);){

			while(fileInput.hasNextLine()){
				String line = fileInput.nextLine();
				String[] parseStr = line.split(" ");

				ArrayList<String> strList = new ArrayList<>();

				for(int i=0; i < parseStr.length; i++){
					if(!parseStr[i].equals(str) && !parseStr[i].isEmpty()){
						strList.add(parseStr[i].trim());
					}
				}
				if(strList.size() == 0 ){
					continue;
				}
				fileOutput.println(String.join(" ",strList));
			}
		} catch (IOException ex){
			System.out.println(ex.getMessage());
		}
	}
}
