/*
    ArrayIndexOutOfBoundsException
*/
package ex_12_2;

import java.util.Scanner;

public class EX_12_2 {
    
    static Scanner input = new Scanner(System.in);

    public static void main(String[] args) {
        String[] months = {"January","February","March","April","May","June","July","August","September","October","November","December"};
        int[] dom = {31,28,31,30,31,30,31,31,30,31,30,31};
        
        try{
            System.out.print("Enter an integer between 1 and 12: ");
            int month = input.nextInt();
            if(month <1 || month >12){
                throw new ArrayIndexOutOfBoundsException(String.valueOf(month));
            }
            System.out.printf("The last day of (%s) is %d\n",months[month-1],dom[month-1]);
        }
        catch (ArrayIndexOutOfBoundsException ex) {
            System.out.println("Wrong number: "+ex.getMessage());
        }
        
        
    }
    
}
