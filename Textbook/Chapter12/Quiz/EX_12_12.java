import java.util.Scanner;
import java.io.PrintWriter;
import java.io.File;
import java.io.IOException;

public class EX_12_12 {
	public static void main(String[] args){
		if(args.length == 0){
			System.out.println("Argument(filename) required");
			return;
		}

		String newFile = "_"+args[0];
		File file = new File(args[0]);

		try( 
				Scanner fileInput = new Scanner(file);
				PrintWriter fileOutput = new PrintWriter(newFile);
		){

			if(!fileInput.hasNextLine()){
				System.out.println("There is no Text in the file");
				return;
			}

			String line1 = "";
			String line2 = fileInput.nextLine();
			while(fileInput.hasNextLine()){
				line1 = line2;
				line2 = fileInput.nextLine();

				if(line2.trim().isEmpty()){
					fileOutput.println(line1);
					continue;
				}

				if(line2.trim().charAt(0) == '{'){
					fileOutput.println(line1 + "{");
					line2 = line2.replaceFirst("\\{"," ");
				} else if(line1.trim().isEmpty()){
					continue;
				} else {
					fileOutput.println(line1);
				}
			}
			fileOutput.println(line2);

		} catch (IOException ex){
			System.out.println(ex.getMessage());
		}
	}
}
