import java.util.ArrayList;

public class LIFO<T>{
	private ArrayList<T> storage = new ArrayList<>();
	public void push(T element){
		storage.add(element);
	}
	public T pop() throws IndexOutOfBoundsException {
		if(storage.size() ==0){
			throw new IndexOutOfBoundsException("LIFO is empty");
		}
		return storage.remove(storage.size()-1);
	}
	public T peek(){
		return storage.get(storage.size()-1);
	}
	public int size(){
		return storage.size();
	}
	public String toString(){
		String line= "";
		int count = 0;
		for(T element : storage){
			line += (count++==0?"":", ") + element;
		}
		return line;
	}
}	
