import java.util.ArrayList;
import java.util.Scanner;
import java.io.File;
import java.io.IOException;

public class ProgCalc {

    static LIFO<Double> stack = new LIFO<>();
    static Scanner input = new Scanner(System.in);

    public void calc(String fileName) {
        try(Scanner fileInput = new Scanner(new File(fileName))){
            while(fileInput.hasNextLine()){
				try{
					String dataLine = fileInput.nextLine();
					
					if(isDouble(dataLine)){
						continue;
					}

					if(isQuestion(dataLine)){
						continue;
					}

					if(dataLine.equals("pop")){
						stack.pop();
						continue;
					}

					if(isOperator(dataLine)){
						continue;
					}

				} catch(IllegalArgumentException ex) {
					System.out.println("warning: invalid line, skipping: "+ex.getMessage());
				}
            }
        } catch(IOException ex){
			System.out.println("Can't open File:" +fileName);
			System.exit(1);
        }

    }

	private boolean isDouble(String dataLine){
		try {
			double number = Double.parseDouble(dataLine);
			stack.push(number);
			return true;
		} catch (NumberFormatException ex){
			return false;
		}

	}

	private boolean isQuestion(String dataLine){
		//if(dataLine.charAt(0) == '?') { // Null: IndexOutOfBoundaryException
		
		if(dataLine.matches("\\?:.*")){

			String[] data = dataLine.split(":",2);
			if(data.length != 2) {
				throw new IllegalArgumentException(dataLine);
			}

			System.out.printf("%s: ",data[1]);
			double num = inputDouble();
			stack.push(num);
			return true;
		} else {
			return false;
		}

	}

    private double inputDouble(){
        while(true) {
            try {
                double number = input.nextDouble();
                input.nextLine();

                return number;
            } catch (NumberFormatException ex) {
                input.nextLine();
                System.out.print("Invalid number, try again: ");
            }
        }
    }

	private boolean isOperator(String dataLine){
		double num1, num2;

		switch (dataLine) {
			case "+":
				num1 = stack.pop();
				num2 = stack.pop();
				stack.push(num1 + num2);
				return true;
			case "-":
				num1 = stack.pop();
				num2 = stack.pop();
				stack.push(num1 - num2);
				return true;
			case "*":
				num1 = stack.pop();
				num2 = stack.pop();
				stack.push(num1 * num2);
				return true;
			case "/":
				num1 = stack.pop();
				num2 = stack.pop();
				if (num2 == 0) {  // FIXME :  double 0 is not 0 after pop
					throw new IllegalArgumentException(dataLine);
				}
				stack.push(num2 / num1);
				return true;
			case "=":
				System.out.println(stack.peek());
				return true;
			default :
				throw new IllegalArgumentException(dataLine);
		}
	}

}
