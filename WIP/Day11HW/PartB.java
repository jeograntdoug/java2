import java.util.ArrayList;
import java.util.Scanner;
import java.io.File;

public class PartB  {
	
	static ArrayList<String> txtList = new ArrayList<>();
	static Scanner input = new Scanner(System.in);

	public static void main(String[] args){

		String filePath = "./";
		File file = new File(filePath);

		for(String fileName : file.list()){
			if(fileName.matches(".*\\.txt")){
				txtList.add(fileName);
			}
		}
		if(txtList.isEmpty()){
			System.out.println("There is no txt file in current directory");
			return;
		}

		System.out.println("Please choose program to execute:");
		int count = 1;
		for(String txtFile : txtList){
			System.out.printf("%d. %s\n",count++,txtFile);
		}
		System.out.print("Enter your choice: ");
		int choice = input.nextInt();

		ProgCalc myCalc = new ProgCalc();
		myCalc.calc(txtList.get(choice-1));

	}
}
