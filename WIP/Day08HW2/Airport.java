public class Airport {
	private String code, city;
	private double latitude,longitude;

	public Airport (String code, String city, double latitude, double longitude) throws ParameterInvalidException {
		setCode(code);
		setCity(city);
		setLatitude(latitude);
		setLongitude(longitude);
	}
	public Airport (String dataLine) throws ParameterInvalidException{

		try {
			String[] data = dataLine.split(";");

			if(data.length !=4 ){
				throw new ParameterInvalidException("Wrong number of data: "+dataLine);
			}

			setCode(data[0]);
			setCity(data[1]);

			//NullPointerException,NumberFormatException
			setLatitude(Double.parseDouble(data[2]));
			setLongitude(Double.parseDouble(data[3]));

		} catch (NullPointerException | NumberFormatException ex){
			throw new ParameterInvalidException("Invalid Number: "+ dataLine);
		}
	}	

	public String getCode(){
		return code;
	} 
	public String getCity(){
		return city;
	} 
	public double getLatitude(){
		return latitude;
	} 
	public double getLongitude(){
		return longitude;
	} 

	public void setCode(String code) throws ParameterInvalidException{
		if(code.isEmpty() || !code.matches("[A-Z]{3}")){
			throw new ParameterInvalidException("Airport code is invalid");
		}
		this.code = code;
	} 
	public void setCity(String city) throws ParameterInvalidException{
		if(city.isEmpty() || city.contains(";")){
			throw new ParameterInvalidException("Airport code is invalid");
		}
		this.city = city;
	} 
	public void setLatitude(double latitude) throws ParameterInvalidException{
		if(latitude <-90 || latitude >90){
			throw new ParameterInvalidException(String.format("Latitude %.2f is invalid",latitude));
		}
		this.latitude = latitude;
	} 
	public void setLongitude(double longitude) throws ParameterInvalidException{
		if(longitude <-180|| longitude >180){
			throw new ParameterInvalidException(String.format("Longitude %.2f is invalid",longitude));
		}
		this.longitude = longitude;
	} 

	public String toFileString(){
		return String.format("%s;%s;%f;%f",code,city,latitude,longitude);
	}

	public String toString(){
		return String.format("%s airport is in %s(%.2f,%.2f)"
								,code,city,latitude,longitude);
	}	

}
