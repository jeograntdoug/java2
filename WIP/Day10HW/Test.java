import java.util.Scanner;

public class Test {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args){
		double d1 = 1.1;
		double d2 = 2.2;
		double d3 = d2 - d1;
		double d4 = 0;

		System.out.println(d1*2 == d2?"d1*2 == d2":"d1*2 != d2");
		System.out.println(d3 == d1?"d3 == d1":"d3 != d1");
		System.out.println((d3-d1) == 0?"d3-d1 == 0":"d3-d1 != 0");
		System.out.println(d4 == 0?"d4 == 0":"d4 != 0");
		System.out.print("Enter 0: ");
		double d5 = input.nextDouble();
		System.out.println(d5 == 0?"d5 == 0":"d5 != 0");
		if(d5 == 0){
			System.out.println("d5 == 0");
		} else {
			System.out.println("d5 != 0");
		}
		print();

	}
	static void print(){
		double d5 = input.nextDouble();
		System.out.println(d5 == 0?"d5 == 0":"d5 != 0");
		if(d5 == 0){
			System.out.println("d5 == 0");
		} else {
			System.out.println("d5 != 0");
		}
	}
}
