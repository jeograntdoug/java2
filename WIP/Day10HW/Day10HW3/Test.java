public class Test {
	public static void main(String[] args){
		LIFO<String> strStack = new LIFO<>();

		strStack.push("first");
		strStack.push("second");
		strStack.push("third");

		System.out.println("=============LIFO");
		System.out.println(strStack);
		System.out.println(strStack.pop());
		System.out.println(strStack.pop());
		System.out.println(strStack.pop());


		FIFO<String> strQueue= new FIFO<>();

		strQueue.add("first");
		strQueue.add("second");
		strQueue.add("third");

		System.out.println("=============FIFO");
		System.out.println(strQueue);
		System.out.println(strQueue.remove());
		System.out.println(strQueue.remove());
		System.out.println(strQueue.remove());
		System.out.println(strQueue.remove());
	}
}
