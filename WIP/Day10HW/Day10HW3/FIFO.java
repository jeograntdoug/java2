import java.util.ArrayList;

public class FIFO<T> {
	private ArrayList<T> storage = new ArrayList<>();

	public void add(T element){
		storage.add(element);
	}

	public T remove() throws IndexOutOfBoundsException {
		if(storage.size() == 0){
			throw new IndexOutOfBoundsException("There is no element");
		}
		return storage.remove(0);
	}
	public int size(){
		return storage.size();
	}

	public String toString(){
		String line = "";
		int count = 0;
		for(T element : storage){
			line += (count==0?"":", ") + element;
			count++;
		}
		return line;
	}
	// also implement toString that prints all items in a single line, comma-separated
}
