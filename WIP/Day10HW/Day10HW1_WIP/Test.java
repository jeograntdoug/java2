import java.util.Scanner;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Test {

	static ArrayList<Number> numberList = new ArrayList<>();

	static ArrayList<Integer> intList = new ArrayList<>();
	static ArrayList<Integer> uniqIntList = new ArrayList<>();

	public static void main(String[] args){
		try( Scanner fileInput = new Scanner(new File("number.txt"));){

			while(fileInput.hasNextInt()){
				int input = fileInput.nextInt();

				if(!intList.contains(input)){
					uniqIntList.add(input);
				}

				intList.add(input);
			}

			for(int num : uniqIntList){
				int freq = Collections.frequency(intList,num);
				numberList.add(new Number(num,freq));
			}

			Collections.sort(numberList,Number.ReverceCompareFreq);

			System.out.printf("Number | Occurrences\n");
			System.out.printf("-------+------------\n");
			for(Number numClass : numberList){
				System.out.printf("%6d | %11d\n",numClass.number,numClass.freq);
			}

		} catch(IOException ex){
			System.out.println("Cannot open the file");
			return;
		}

	}
}

class Number {
	int number;
	int freq;

	Number (int number, int freq){
		this.number = number;
		this.freq = freq;
	}

	static Comparator<Number> ReverceCompareFreq = new Comparator<>(){
		public int compare(Number n1, Number n2){
			return n2.freq-n1.freq;
		}
	};
}
