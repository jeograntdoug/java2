import java.util.Scanner;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Test_old {

	static ArrayList<Number> numberList = new ArrayList<>();

	public static void main(String[] args){
		try( Scanner fileInput = new Scanner(new File("number.txt"));){

			while(fileInput.hasNextInt()){
				int num = fileInput.nextInt();
				Number newNumber = new Number(num);
				int index = Collections.binarySearch(numberList ,newNumber);

				if( index < 0 ){	//If the number is not in the list, add
					numberList.add(newNumber);
				}else {	//If the number is in the list, increase freq by 1
					numberList.get(index).freq++;
				}
			}

		} catch(IOException ex){
			System.out.println("Cannot open the file");
			return;
		}

		System.out.printf("Number | Occurrences\n");
		System.out.printf("-------+------------\n");
		for(Number num : numberList){
			System.out.printf("%6d | %11d\n",num.number,num.freq);
		}

	}
}

class Number implements Comparable<Number>{
	int number;
	int freq;

	Number (int number){
		this.number = number;
		freq = 1;
	}

	@Override
	public int compareTo(Number numClass){
		return this.number - numClass.number;
	}
	public boolean equals(Number num){
		return this.number == num.number;
	}

	static Comparator<Number> CompareFreq = new Comparator<>(){
		public int compare(Number n1, Number n2){
			return n1.freq-n2.freq;
		}
	};

	static Comparator<Number> CompareNumber= new Comparator<>(){
		public int compare(Number n1, Number n2){
			return n1.number-n2.number;
		}
	};
}
