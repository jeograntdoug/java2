public class Hexagon extends GeoObj {
	private double edgeLength;

	public Hexagon (String color,double edgeLength) throws DataInvalidException{
		super(color);
		setEdgeLength(edgeLength);
	}	

	public double getEdgeLength(){
		return edgeLength;
	}
	public void setEdgeLength(double edgeLength){
		this.edgeLength = edgeLength;
	}

	@Override
	public double getSurface(){
		return 3/2*Math.sqrt(3)*edgeLength*edgeLength;
	}

	@Override
	public double getCircumPerium(){
		return 6*edgeLength;
	}

	@Override
	public int getVericesCount(){
		return 6;
	}

	@Override
	public int getEdgesCount(){
		return 6;
	}


	@Override
	public String toString(){
		return String.format("Sphere: %s color, Edge Length: %.2f, Surface: %.2f Perimater : %.2f Verices: %d Edges: %d",getColor(),edgeLength,getSurface(),getCircumPerium(),getVericesCount(),getEdgesCount());
	}
}

