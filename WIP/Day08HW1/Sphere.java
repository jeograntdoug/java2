public class Sphere extends GeoObj {
	private double radius;

	public Sphere (String color,double radius) throws DataInvalidException{
		super(color);
		setRadius(radius);
	}	

	public void setRadius(double radius){
		this.radius= radius;
	}
	public double getRadius(){
		return radius;
	}


	@Override
	public double getSurface(){
		return 4*Math.PI * radius* radius;
	}

	@Override
	public double getCircumPerium(){
		return getSurface();
	}

	@Override
	public int getVericesCount(){
		return 0;
	}

	@Override
	public int getEdgesCount(){
		return 1;
	}

	@Override
	public String toString(){
		return String.format("Sphere: %s color, Radius: %.2f, Surface: %.2f Perimater : %.2f Verices: %d Edges: %d",getColor(),radius,getSurface(),getCircumPerium(),getVericesCount(),getEdgesCount());
	}
}

