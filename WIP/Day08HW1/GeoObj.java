public abstract class GeoObj {
	private  String color;

	public GeoObj(String color) throws DataInvalidException{
		setColor(color);
	}

	public void setColor(String color) throws DataInvalidException {
		if(!color.matches("[a-zA-Z -]+")){
			throw new DataInvalidException("Color["+ color +"] is invalid!");
		}
		this.color = color;
	}
	public String getColor(){
		return color;
	}

	abstract double getSurface();
	abstract double getCircumPerium();
	abstract int getVericesCount();
	abstract int getEdgesCount();

	static GeoObj createFromLine(String dataLine) throws DataInvalidException {
		String[] data = dataLine.split(";");

		try{
			switch(data[0]){
				case "Point" :
					return new Point(data[1]);
				case "Rectangle" :
					return new Rectangle(data[1]
										,Double.parseDouble(data[2])
										,Double.parseDouble(data[3]));
				case "Square" :
					return new Square(data[1],Double.parseDouble(data[2]));
				case "Circle" :
					return new Circle(data[1],Double.parseDouble(data[2]));
				case "Sphere" :
					return new Sphere(data[1],Double.parseDouble(data[2]));
				case "Hexagon" :
					return new Hexagon(data[1],Double.parseDouble(data[2]));
				default :
					throw new IllegalArgumentException("Invalid data line:"+dataLine);
			}
			//Double.parseDouble throws NumberFormatException(child of IllegalArgumentException) 
		} catch (NullPointerException | IllegalArgumentException
			   | ArrayIndexOutOfBoundsException |DataInvalidException ex) {
			throw new DataInvalidException("Invalid data line:"+dataLine);
		}
	}
}
