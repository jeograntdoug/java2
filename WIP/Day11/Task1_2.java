import java.io.File;
import java.io.IOException;
import java.util.*;

public class Task1_2 {
    static HashMap<Integer,Integer> map = new HashMap<>();

    public static void main(String[] args) {
        try(Scanner fileInput = new Scanner(new File("number.txt"))) {
            while(fileInput.hasNextInt()) {
                int num = fileInput.nextInt();
                if(!map.containsKey(num)) {
                    map.put(num,1);
                    continue;
                }

                int count = map.get(num);
                map.put(num,++count);
            }
            Integer[] numArray = map.keySet().toArray(new Integer[0]);

            for (int num : numArray){
                System.out.printf("%6d | %10d\n",num,map.get(num));
            }

            System.out.println("===========================");
            map = sortByKey(map);
            numArray = map.keySet().toArray(new Integer[0]);

            for (int num : numArray){
                System.out.printf("%6d | %10d\n",num,map.get(num));
            }
        } catch(IOException ex) {
            System.out.println("Cannot open file");
        }

    }

    static HashMap<Integer,Integer> sortByKey(HashMap<Integer,Integer> map){
        List <Map.Entry<Integer,Integer>> list = new LinkedList<Map.Entry<Integer, Integer>>(map.entrySet());

        Collections.sort(list, new Comparator<Map.Entry<Integer,Integer>>(){
            public int compare(Map.Entry<Integer,Integer> o1,
                               Map.Entry<Integer,Integer> o2){
                return o2.getValue().compareTo(o1.getValue());
            }
        });

        HashMap<Integer,Integer> temp = new LinkedHashMap<Integer,Integer>();
        for(Map.Entry<Integer,Integer> aa : list){
            temp.put(aa.getKey(),aa.getValue());
        }
        return temp;
    }
}

